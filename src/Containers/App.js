import React from 'react';
import CardList from '../Components/CardList';
import SearchBox from '../Components/SearchBox'
import Scroll from '../Components/Scroll'
import ErrorBoundry from '../Components/ErrorBoundry'

// Good practice to start the web page with empty data (robots)
// then update it with componentDidMount()
class App extends React.Component{
    constructor(){
        super()
        this.state={
            robots: [],
            searchfield: '',
        }
    }

    // Build in function for React so arrow function isn't needed
    componentDidMount(){
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response=> {
            return response.json();
        })
        .then(users =>{
            this.setState({robots: users})
        })
        
    }

    // ALWAYS use arror function for user functions
    onSearchChange = (event) =>{
        this.setState({searchfield: event.target.value})
    } 

    render(){
        const filterRobots = this.state.robots.filter(robot =>{
            return robot.name.toLowerCase().includes(this.state.searchfield.toLowerCase())
        })

        /* Display loading if no element was added to empty array*/
        if(this.state.robots.length === 0){
            return <h1> Loading </h1>
        }else{
            return (
                <div className= 'tc'>
                    <h1>RoboFriends</h1>
                    <SearchBox SearchChange = {this.onSearchChange}/>
                    {/* Components that surrounds another component treats the inner 
                    component like a child/children. So functions added to the parent
                    will only affect the child. Therefore, only CardList will be scrollable*/}
                    <Scroll>
                        <ErrorBoundry>
                            <CardList robots = {filterRobots}/>
                        </ErrorBoundry>
                    </Scroll>
                       
                </div>   
            );
        }
    }
}

export default App;