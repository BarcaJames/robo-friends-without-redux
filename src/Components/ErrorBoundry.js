import React, { Component } from 'react';

/**For error handling */
class ErrorBoundry extends Component {
    constructor() {
        super();
        this.state = {
            hasError: false,
        }
    }

    /**Build in function */
    componentDidCatch(error, info){
        this.setState({hasError: true});
    }

    /**If error was found render/display h1 tag else render normally */
    render(){
        if(this.state.hasError){
            return <h1>Oooops. That is not good</h1>
        }else{
            return this.props.children
        }
    }
}

export default ErrorBoundry;