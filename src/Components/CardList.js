import React from 'react';
import Card from './Card.js';

const CardList = ({robots}) => {
    // Used map to traverse all items in robots.js
    // ALWAYS give elements in the array a key attribute

    const cardArray = robots.map((user, i) => {
        return < Card 
            key = {i} 
            id ={robots[i].id} 
            name={robots[i].name} 
            email={robots[i].email}
        / >
    });

    return(
        <div>
        {/* Call function here to return array of cards */}
            {cardArray}
        </div>
    );

}

export default CardList;