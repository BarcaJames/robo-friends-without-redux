import React from 'react'

/**style = {{}} is the syntax for inline css 
 * so the scrollable will only be added to 
 * props.children which is CardList
*/
const Scroll = (props) =>{
    return(
        <div style = {{overflowY: 'scroll', border: '5px solid black', height: '800px' }}>
            {props.children}
        </div>
        
    );
}

export default Scroll;