import React from 'react';
// haha used to past something to a function 
const Card = (haha) => {
    return (
        <div className="tc bg-light-green dib br3 ma2 pa3 grow bw2 shadow-5">
            {/* Robohash is a URL with random robots */}
            <img alt = "Robots" src={`https://robohash.org/${haha.id}?200*200`}/>
            <div>
                <h1> {haha.name} </h1>
                <p> {haha.name} </p>
            </div>
        </div> 
    );
}

export default Card;